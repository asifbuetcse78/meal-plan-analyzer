﻿using System;
using System.Linq;

namespace chaldal
{
    internal partial class Program
    {
        public static string DirectoryPath = "../data/";

        public static void ServeData(string command, long startDate, long endDate)
        {
            var dayIdWithinSpecifiedDate = DataBase.CalendarList
                .Where(e => long.Parse(e.Date.Replace("-", string.Empty).ToString()) >= startDate &&
                            long.Parse(e.Date.Replace("-", string.Empty).ToString()) <= endDate).Select(f => f.Id)
                .ToList();

            var dayIdBeforeSpecifiedDate = DataBase.CalendarList
                .Where(e => long.Parse(e.Date.Replace("-", string.Empty).ToString()) < startDate).Select(f => f.Id)
                .ToList();

            switch (command)
            {
                case "active":
                {
                    Console.WriteLine(string.Join(",", DataBase.MealList.Where(e => dayIdWithinSpecifiedDate.Contains(e.CalendarId))
                        .GroupBy(e => e.UserId).Where(grp => grp.Count() >= 5 && grp.Count() <= 10).Select(p => p.Key).OrderBy(e => e)
                        .ToList()));
                    break;
                }
                case "superactive":
                {
                    Console.WriteLine(string.Join(",", DataBase.MealList.Where(e => dayIdWithinSpecifiedDate.Contains(e.CalendarId))
                        .GroupBy(e => e.UserId).Where(grp => grp.Count() > 10).Select(p => p.Key)
                        .ToList()));
                    break;
                }
                case "bored":
                {
                    var activePrevious =  DataBase.MealList.Where(e => dayIdBeforeSpecifiedDate.Contains(e.CalendarId))
                        .GroupBy(e => e.UserId).Where(grp => grp.Count() >= 5 && grp.Count() <= 10).Select(p => p.Key)
                        .ToList();
                    var notActiveNow =  DataBase.MealList.Where(e => dayIdWithinSpecifiedDate.Contains(e.CalendarId))
                        .GroupBy(e => e.UserId).Where(grp => grp.Count() < 5).Select(p => p.Key)
                        .ToList();
                    Console.WriteLine(string.Join(",", activePrevious.Intersect(notActiveNow)));
                    break;
                }
            }
        }
        private static int Main(string[] args)
        {
            if (args.Length < 3)
            {
               Console.WriteLine("Sorry; command parameter mismatch");
               return 0;
            }
            try
            {
                LoadData();
            }
            catch (Exception e)
            {
                Console.WriteLine("Issue in one of the files. The issue is: "+ e.Message);
            }

            try
            {
                ServeData(args[0], long.Parse(args[1]), long.Parse(args[2]));
            }
            catch (Exception e)
            {
                Console.WriteLine("Issue with parameter format. The issue is: " + e.Message);
            }

            Console.WriteLine("press any key to continue...");
            Console.ReadKey();
            return 0;
        }
    }
}
