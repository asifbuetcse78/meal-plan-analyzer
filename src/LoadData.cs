﻿using System.IO;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace chaldal
{
    internal partial class Program
    {
        private static void ParseCalendar(dynamic calendarData)
        {
            foreach (var cal in calendarData)
            {
                if (DataBase.CalendarList.Any(e => e.Date == cal.Name && e.Id == cal.Value.Value)) continue;
                DataBase.CalendarList.Add(new Calendar { Date = cal.Name, Id = cal.Value.Value });
            }
        }

        private static void ParseUser(long id, long earliestDate)
        {
            DataBase.UserList.Add(new User
            {
                Id = id,
                EarliestDate = earliestDate
            });
        }

        private static void ParseDishToMeal(dynamic dishToMeal)
        {
            foreach (var dishMeal in dishToMeal)
                DataBase.DishToMealList.Add(new DishToMeal
                { DishId = long.Parse(dishMeal.Name), MealId = dishMeal.Value.Value });
        }

        private static void ParseMeal(dynamic mealObject, dynamic daysWithDetails, long userId)
        {
            var meal = new Meal
            {
                Id = long.Parse(((JValue)((JContainer)((JContainer)mealObject).First).First).Value.ToString()),
                CalendarId = long.Parse(((JProperty)daysWithDetails).Name),
                UserId = userId
            };
            foreach (var mealProperties in ((JContainer)mealObject).Children())
            {
                if (((JProperty)mealProperties).Name == "type")
                {
                    meal.Type = ((JValue)((JProperty)mealProperties).Value).Value.ToString();
                }
            }
            DataBase.MealList.Add(meal);
        }

        private static void ParseDish(dynamic dishList)
        {
            foreach (var dishIterator in dishList)
            foreach (var dishProperties in ((JContainer) dishIterator).Children())
                if (DataBase.DishList.All(e =>
                    e.Id != long.Parse(
                        ((JValue) ((JProperty) ((JContainer) dishProperties).First).Value).Value.ToString())))
                    DataBase.DishList.Add(new Dish
                    {
                        Id = long.Parse(((JValue) ((JProperty) ((JContainer) dishProperties).First).Value).Value
                            .ToString()),
                        Name = ((JValue) ((JProperty) ((JContainer) dishProperties).Last).Value).Value.ToString()
                    });
        }

        private static void ParseData(dynamic mainJsonContent, long userId)
        {
            ParseCalendar(mainJsonContent.calendar.dateToDayId);
            ParseUser(userId, DataBase.CalendarList.First(e => e.Date == mainJsonContent.earliestDate.Value).Id);
            ParseDishToMeal(mainJsonContent.calendar.dishIdToMealId);
            foreach (var singleDayWithDetails in mainJsonContent.calendar.daysWithDetails)
            {
                foreach (var singleMealWithDetails in singleDayWithDetails.Value.details.mealsWithDetails)
                {
                    ParseMeal(singleMealWithDetails.Value.meal, singleDayWithDetails, userId);
                    ParseDish(singleMealWithDetails.Value.details.dishes);
                }
            }
        }

        public static void LoadData()
        {
            foreach (var documentFile in Directory.GetFiles(DirectoryPath))
                ParseData(JObject.Parse(File.ReadAllText(documentFile)), int.Parse(documentFile.Split('/').Last().Split('.').First()));
        }
    }
}
