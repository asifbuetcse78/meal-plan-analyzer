﻿using System.Collections.Generic;

namespace chaldal
{
    public static class DataBase
    {
        public static List<User> UserList = new List<User>();
        public static List<Calendar> CalendarList = new List<Calendar>();
        public static List<DishToMeal> DishToMealList = new List<DishToMeal>();
        public static List<Meal> MealList = new List<Meal>();
        public static List<Dish> DishList = new List<Dish>();
    }
    public class Dish
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class Meal
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public long CalendarId { get; set; }
        public string Type { get; set; }
    }

    public class DishToMeal
    {
        public long MealId { get; set; }
        public long DishId { get; set; }
    }

    public class User
    {
        public long Id { get; set; }
        public long EarliestDate { get; set; }
    }

    public class Calendar
    {
        public long Id { get; set; }
        public string Date { get; set; }
    }
}
