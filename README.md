Language Used: C#


## Task
Build a program to analyze app usage data for a hypothetical menu planning calendar app. On any day, a user can plan multiple meals, and each meal may have multiple dishes. The program you build will analyze how engaged users are with the app.

## Data
In the ./data folder, you will find a bunch of files in the form userId.json. They contain app data for a bunch of users. The structure should be fairly obvious upon examination.

## Output
./dist/run active 2016­09­01 2016­09­08

should result in a list of comma­ separated user ids that were “active” during the specified period, where “active” means they had at least 5 meals.

./dist/run superactive 2016­09­01 2016­09­08

should result in a list of comma­ separated user ids that were “super­active” during the specified period, which means they had more than 10 meals.

./dist/run bored 2016­09­01 2016­09­08

should result in a list of comma­ separated user ids that were “bored” during the specified period, meaning that they were “active” in the preceding period, but didn’t make the “active” threshold in the specified period.

# Instructions

open commant prompt

cd into the directory /src

run command:  csc -out:../dist/run.exe Program.cs LoadData.cs DataModel.cs /reference:Newtonsoft.Json.dll

(if - csc is not recognized error - add the cse.exe file path to the environment variable)

run command: move Newtonsoft.Json.dll ../dist

cd into the directory /dist

run command: run active 2016­09­01 2016­09­08

run command: run superactive 2016­09­01 2016­09­08

run command: run bored 2016­09­01 2016­09­08


## Assumption
As the sample data size is fairly small; no database was used. Instead; data is loaded in memory.
But the data structure is designed in such a way; that if need for persistent data storage arises;
the database insertion query can be easily incorporated in the structure. 